# DIGITALIZACION
Repo GIT S2I para OpenShift de Modulo de Digitalizacion de SOADOC

## Version
Modulo de Digitalizacion ajustado para Oracle, para trabajar en contenedores OpenShift y Variable JAVA_OPTS_APPEND

## JAVA_OPTS_APPEND

-Dspring.profiles.active=dev 
-Decm.app.resources=/opt/eap/modules/tmp 
-Ddocmanager.app.resources=/opt/eap/modules/tmp 
-Dfile.encoding=Cp1252 
-Dsum.jnu.encoding=Cp1252 
-Dpath-log-app=./logs/ 
-Durl-digitalizacion-api=http://digitalizacion-soadocdes.cloudapps.sic.gov.co 
-Durl-soadoc-api=http://servicebusiness-soadocdes.cloudapps.sic.gov.co

# MODELO REPOSITORIO S2I PARA CONTENEDORES

## .s2i

## configuration

## deployments

## extensions
